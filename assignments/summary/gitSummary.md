## What is Git?
Git is the most commonly used version control system today and is quickly becoming the standard for version control. Git is a distributed version control system, meaning your local copy of code is a complete version control repository. These fully-functional local repositories make it is easy to work offline or remotely. You commit your work locally, and then sync your copy of the repository with the copy on the server. This paradigm differs from centralized version control where clients must synchronize code with a server before creating new versions of code.

Git’s flexibility and popularity make it a great choice for any team. Many developers and college graduates already know how to use Git. Git’s user community has created many resources to train developers and Git’s popularity make it easy to get help when you need it. Nearly every development environment has Git support and Git command line tools run on every major operating system.

## Git basics

Every time you save your work, Git creates a commit. A commit is a snapshot of all your files at a point in time. If a file has not changed from one commit to the next, Git uses the previously stored file. This design differs from other systems which store an initial version of a file and keep a record of deltas over time.

![](https://docs.microsoft.com/en-us/azure/devops/learn/_img/linear_straight_line.png)

### Branches

Each developer saves changes their own local code repository. As a result, you can have many different changes based off the same commit. Git provides tools for isolating changes and later merging them back together. Branches, which are lightweight pointers to work in progress, manage this separation.  Once your work created in a branch is finished, merge it back into your team’s main (or master) branch.

![](https://docs.microsoft.com/en-us/azure/devops/learn/_img/branching_line.png)

### Files and commits

Files in Git are in one of three states: modified, staged, or committed. When you first modify a file, the changes exist only in your working directory. They are not yet part of a commit or your development history. You must stage the changed files you want to include in your commit. The staging area contains all changes that you will include in your next commit. Once you’re happy with the staged files, commit them with a message describing what changed. This commit becomes a part of your development history.

![](https://docs.microsoft.com/en-us/azure/devops/learn/_img/file_status_lifecycle.2.png)

## Benefits of Git
-Simultaneous development
-Faster releases
-Built-in integration
-Strong community support
-Git works with your team

## Install and set up Git
### Windows
Download and install Git for Windows. Once installed, you’ll be able to use Git from the command prompt or PowerShell.
### macOS
> $ brew install git
### Linux
> $ sudo apt-get install git

### Basic commands for Git:

1. Configuration: To configure git in machine.
> $ git config  --global user.name 'name-of -the-user'
> $ git config --global user.email 'email-id'

2. Initialize: To create .git directory in machine.
> $ git init

3. Clone: Cloning the remote repo to local repo.    
> $ git clone (link-to-repo)

4. Status: To check the status of files in staging area.  
Check the status of files in staging area.
> $ git status <this will show either files are staged or committed>

5. Add: To add the files to staging area.  
To add the files to the staging area.
> $ git add . <this dot will add all the files to staging area.>

6. push: To push the file from local repo to remote repo.
> $ git push origin <branch-name>