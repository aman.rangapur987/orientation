## What is Docker?
Docker is a software platform for building applications based on containers — small and lightweight execution environments that make shared use of the operating system kernel but otherwise run in isolation from one another.
 
![](https://upload.wikimedia.org/wikipedia/commons/4/4e/Docker_%28container_engine%29_logo.svg)
## Terminologies of Docker
### Docker Hub
A public registry to upload images and work with them. Docker Hub provides Docker image hosting, public or private registries, build triggers and web hooks, and integration with GitHub and Bitbucket.

### Docker Containers
A container is a standard unit of software that packages up code and all its dependencies so the application runs quickly and reliably from one computing environment to another.

![](https://images.idgesg.net/images/article/2017/06/virtualmachines-vs-containers-100727624-large.jpg)

### Dockerfile
A text file that contains instructions for building a Docker image. It's like a batch script, the first line states the base image to begin with and then follow the instructions to install required programs, copy files, and so on, until you get the working environment you need.

### Docker Image
A Docker image is the set of processes outlined in the Docker file.

![](https://cdn-images-1.medium.com/max/1600/1*p8k1b2DZTQEW_yf0hYniXw.png)

## Containers and Virtual Machines

![](https://pediaa.com/wp-content/uploads/2018/10/Difference-Between-Container-and-VM-Comparison-Summary-621x1024.jpg)

### Representation of Containers and Virtual Machines
![](https://www.adminschoice.com/wp-content/uploads/2018/06/container-vs-vms.png)


## Basic Docker Commands
1. Pull:
> $ Docker pull (image name)

2. Push:
> $ docker push (username/imagename)

3. Ps:
> $ docker ps

4. Run:
> $ docker run -it-d (image name)

5. Stop: 
> $ docker stop (container id)

![](https://cdn-images-1.medium.com/max/1200/0*XKTCmmou4fT6os-c.)
